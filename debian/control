Source: ruby-bootsnap
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Sruthi Chandran <srud@disroot.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 2.1~),
               rake,
               rake-compiler,
               rubocop,
               ruby-bundler,
               ruby-minitest (<< 6.0),
               ruby-mocha,
               ruby-msgpack (>= 1.4~),
               ruby-byebug
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-bootsnap.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-bootsnap
Homepage: https://github.com/Shopify/bootsnap
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-bootsnap
Architecture: any
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: Boot large ruby/rails apps faster
 Bootsnap is a library that plugs into Ruby, with optional support for
 `ActiveSupport` and `YAML`, to optimize and cache expensive computations.
 .
 Bootsnap optimizes methods to cache results of expensive computations, and can
 be grouped into two broad categories:
    * Path Pre-Scanning
    * Compilation caching
